if [ "$HIVEPIRATE" != 'HIVEPIRATE' ]; then
	## HIVEPIRATE VAR ##
        # This is for keeping the user from creating nested Hive Pirate instances
        HIVEPIRATE="HIVEPIRATE"
        export HIVEPIRATE

	# Script entrance path
	enterenvpath=$(realpath $0)
	enterenvpath=$(dirname $enterenvpath)
	cd $enterenvpath

	if [ `id -u` != 0 ]; then
		echo "Please enter root password to enter environment"
		su -c "bash --rcfile $enterenvpath/hivepiraterc -i"
	else
	        su -c "bash --rcfile $enterenvpath/hivepiraterc -i"
        fi
elif [ "$HIVEPIRATE" == 'HIVEPIRATE' ]; then
        echo "HIVEPIRATE variable is set, if you want to nest instances, unset HIVEPIRATE"
	exit 1
fi
