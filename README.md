
# Table of Contents

1.  [Hive Pirate](#hive-pirate)
    1.  [Liability Clause](#liability-clause)
    2.  [About](#about)
    3.  [Basic Vega Tuning](#basic-vega-tuning)


<a id="org22e929b"></a>

# Hive Pirate


<a id="org7f3e189"></a>

## Liability Clause

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

IT MUST ALSO BE STATED THAT THE SCRIPTS, FILES, AND BINARIES INCLUDED WITHIN THIS TOOL SET BELONG TO THEIR RESPECTIVE OWNERS. AN AMOUNT
OF THE SCRIPTS, FILES, AND BINARIES BELONG TO DIFFERENT PARTIES. LICENSING MAY VERY! REDISTRIBUTE AND MAKE MODIFICATIONS AT YOUR OWN
RISK!


<a id="org0a41294"></a>

## About

Hive Pirate is an environment and set of tools that allow a user to easily tune AMD cards using HiveOS-like settings.

Currently, Hive Pirate is in an early development release stage and is likely to not work correctly. The only officially supported AMD
GPUs that work with Hive Pirate are Vega 10 cards. Both the Vega 56 and Vega 64 cards work out of the box just fine.

To begin using Hive Pirate you must run the hive-pirate.sh script in the main directory.  This script will prompt you for a root
password as the tools require root access to interact with the GPUs. (If you have sudo installed and no root user password set please
run with sudo.)

Once you have entered the Hive Pirate environment, you may now start tinkering with your cards using the hp meta tool.  I recommend
running it with &#x2013;help, so you have an idea of what you can do.


<a id="org2778b4d"></a>

## Basic Vega Tuning

To get started, I recommend first saving the PP tables for your cards using:

    hp --save-all-pp-tables

After you save your PP tables, you can now create an OC config for your card using:

    hp --create-oc --card=0

After you create your config, you can now edit the config using: (By default vim is used. I'll add a way to change the default editor
later.)

    hp --edit-oc --card=0

After you save your config, you can now set the OC config to the card by using:

    hp --set-oc --card=0

Now your card is setup! For advanced tuning I recommend playing around a little.

