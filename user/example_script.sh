## Example of a script that can made to manage GPUs with Hive Pirate
## This simple for loop sets the OC configs for 4 vega cards in one go

for i in {0..3} 
do
	hp --set-oc --card=$i
done
